**There are various GPU analysis codes in this directory, written in CUDA.**
   
 * I have added a comment with capital letters "TWEAK THIS" ahead of those variables which you can tweak and see the results
 	 
 * You must have the nvcc compiler installed.

 * To install nvcc compiler in linux, first install recommended nvidia driver and then install cuda with the folloing command
    

 *      sudo apt install nvidia-cuda-toolkit

 *	 Compile code using: 
 *	 	 nvcc filename.cu  -arch=sm_61 -lm -Xptxas -O3,-v --use_fast_math -w

      
      -arch=sm_61 is the flag used for the GPU card architecture. 
      -arch=sm_61 is used for Geforce GTX 1050 Ti (my machine’s GPU). To run above codes on
     Tesla V100, change -arch=sm_61 to -arch=sm_70.



 *	 Run :
 *		 nohup ./a.out &

    **Analysis codes:**  
 1. Pair correlation function<br/>
 2. Structure factor <br/>
 3. Mean square displacement<br/>
 4. Overlap function<br/>
 5. F_s(k,t)<br/>	 			

  Read input Data:<br/>

  Coordinate file   :: 		This code take 501 uncorrelated configurations of Kob Anderson model which was saved after every 10000 MD steps. MD step = 0.02 (Model is first equilibrated up to 10^6 MD step and then data is saved to the file) Change this number according to your configurations<br/>
  Format:<br/><pre>
                     x1      y1       z1    #t=0
                     x2      y2       z2
                     .......
                     xn      yn       zn
                     x1      y1       z1    #t=1
                     x2      y2       z2
                     .......<br/>
                     xn      yn       zn
                     .....
                     .....</pre>


![KAGrAveGPU4000](/uploads/471342ce3f968f53abce09edc59fddd1/KAGrAveGPU4000.png)

![KASFAveGPU4000](/uploads/4393435e758e7b01ec801fcb9737e44b/KASFAveGPU4000.png)

![MSDMDDPDKANewZeta0dot1](/uploads/ba3fd17a1e19db1854260260fdb3c09b/MSDMDDPDKANewZeta0dot1.png)

 Written By::<br/>
       **Rajneesh Kumar**
 * Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 * Bengaluru 560064, India.
 * Email: rajneesh[at]jncasr.ac.in
 * 27 Dec, 2020
   

  

